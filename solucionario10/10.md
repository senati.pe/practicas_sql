# 10. Valores nulos (null)

# practica laboratorio

## Ejercicio 01

### Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".
```sql
 drop table medicamentos;
 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);


```
### Visualice la estructura de la tabla "medicamentos" note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".

```sql
DESCRIBE medicamentos;

```
### salida 
Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3)    

### Ingrese algunos registros con valores "null" para los campos que lo admitan
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);

```
### salida 
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
### vea los registros
```sql
SELECT * FROM medicamentos;
```
### salida
1	Sertal gotas			100
2	Sertal compuesto		8,9	150
3	Buscapina	Roche		200

### Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)
```sql
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (4, '', 'Farmalab', 12.50, 50);

```
### salida 

Error que empieza en la línea: 23 del comando -
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (4, '', 'Farmalab', 12.50, 50)
Error en la línea de comandos : 23 Columna : 85
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("KEMIL"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.

### Ingrese un registro con una cadena de 1 espacio para el laboratorio.
```sql
INSERT INTO medicamentos (codigo, nombre, laboratorio, precio, cantidad) VALUES (4, 'Paracetamol', '                   ', 10.99, 200);

```
### salida 
1 fila insertadas.
1 fila insertadas.

## Ejercicio 02

### Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".
```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);

```
### Visualice la estructura de la tabla. note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".
```sql
DESCRIBE peliculas;
```
### salida
Nombre   ¿Nulo?   Tipo         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3)    

### Ingrese los siguientes registros
```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);

```
### salida 
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.

### Recupere todos los registros para ver cómo Oracle los almacenó.

```sql
SELECT * FROM peliculas;

```
### salida 
1	Mision imposible	               Tom Cruise	120
2	Harry Potter y la piedra filosofal	null	180
3	Harry Potter y la camara secreta	Daniel R.	
0	Mision imposible 2	                 null	150
4	Titanic	                            L. Di Caprio	220
5	Mujer bonita	                  R. Gere.J. Roberts	0

### Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)

```sql
UPDATE peliculas SET duracion = NULL WHERE duracion = 0;

```
### salida 
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
0 filas actualizadas.

### Recupere todos los registros.
```sql
SELECT * FROM peliculas;

```
### salida
Mision imposible	Tom Cruise	120	3
Mision imposible 2	Tom Cruise	180	2
Mujer bonita	Julia R.	90	3
Elsa y Fred	China Zorrilla	90	2
Mision imposible	Tom Cruise	120	3
Mision imposible 2	Tom Cruise	180	2
Mujer bonita	Julia R.	90	3
Elsa y Fred	China Zorrilla	90	2