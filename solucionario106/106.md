# 106. Disparador de borrado (nivel de sentencia y de fila)

# practica laboratorio

## Ejercicio 01

### Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se elimina un registro de la tabla "libros".
Eliminamos la tabla "libros" y la tabla "control":

drop table libros;
drop table control;


### Creamos las tablas con las siguientes estructuras:

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);


### Ingresamos algunos registros en "libros":

insert into libros values(97,'Uno','Richard Bach','Planeta',25);
insert into libros values(98,'El aleph','Borges','Emece',28);
insert into libros values(99,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(100,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(101,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
insert into libros values(102,'El experto en laberintos','Gaskin','Planeta',20);


### Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';