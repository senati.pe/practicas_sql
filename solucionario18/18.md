# 18. Operadores aritméticos y de concatenación (columnas calculadas)

# practica laboratorio

## Ejercicio 01

### Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

```sql
drop table articulos;
create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);

```

### Ingrese algunos registros:

```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);

```
### El comercio quiere aumentar los precios de todos sus artículos en un 15%. Actualice todos los precios empleando operadores aritméticos.


```sql
update articulos
set precio = precio + (precio * 0.15);

```
### Vea el resultado.

```sql
select * from articulos;

```
### Muestre todos los artículos, concatenando el nombre y la descripción de cada uno de ellos separados por coma.

```sql
select nombre || ', ' || descripcion as nombre_descripcion
from articulos;

```
### Reste a la cantidad de todas las impresoras, el valor 5, empleando el operador aritmético menos ("-")

```sql
update articulos
set cantidad = cantidad - 5
where nombre = 'impresora';

```

