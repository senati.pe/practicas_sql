# Practicas_SQL

## estudiante: kemil jimenez condori

## Solucionario

[4. Recuperación de algunos campos (select)](solucionario4/4.md)

[5. Recuperación de registros específicos (select . where)](solucionario5/5.md)

[6. Operadores relacionales](solucionario6/6.md)

[7. Borrar registros (delete)](solucionario7/7.md)

[8. Actualizar registros (update)](solucionario8/8.md)

[9. Comentarios](solucionario9/9.md)

[10. Valores nulos (null)](solucionario10/10.md)

[11. Operadores relacionales (is null)](solucionario11/11.md)

[12. Clave primaria (primary key)](solucionario12/12.md)

[13. Vaciar la tabla (truncate table)](solucionario13/13.md)

[14. Tipos de datos alfanuméricos](solucionario14/14.md)

[15. Tipos de datos numéricos](solucionario15/15.md)

[16. Ingresar algunos campos](solucionario16/16.md)

[17. Valores por defecto (default)](solucionario17/17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](solucionario18/18.md)

[19.  Alias (encabezados de columnas)](solucionario19/19.md)

[20. Funciones string](solucionario20/20.md)

[21. Funciones matemáticas](solucionario21/21.md)

[22. Funciones de fechas y horas](solucionario22/22.md)

[23. Ordenar registros (order by)](solucionario23/23.md)

[24. Operadores lógicos (and - or - not)](solucionario24/)

[25. Otros operadores relacionales (between)](solucionario25/25.md)

[26. Otros operadores relacionales (in)](solucionario26/26.md)

[27. Búsqueda de patrones (like - not like)](solucionario27/27.md)

[28. Contar registros (count)](solucionario28/28.md)

[30. Agrupar registros (group by)](solucionario30/30.md)

[31. Seleccionar grupos (Having)](solucionario31/31.md)

[32. Registros duplicados (Distinct)](solucionario32/32.md)

[33. Clave primaria compuesta](solucionario33/33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](solucionario34/34.md)

[35. Alterar secuencia (alter sequence)](solucionario35/35.md)

[36. Integridad de datos](solucionario36/36.md)

[37. Restricción primary key](solucionario37/37.md)

[38. Restricción unique](solucionario38/38.md)

[39. Restriccioncheck](solucionario39/39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](solucionario40/40.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](solucionario41/41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](solucionario42/42.md)

[43. Indices](solucionario43/43.md)

[44. Indices (Crear . Información)](solucionario44/44.md)

[45. Indices (eliminar)](solucionario45/45.md)

[46. Varias tablas (join)](solucionario46/46.md)

[47. Combinación interna (join)](solucionario47/47.md)

[48. Combinación externa izquierda (left join)](solucionario48/48.md)

[49. Combinación externa derecha (right join)](solucionario49/49.md)

[50. Combinación externa completa (full join)](solucionario50/50.md)

[51. Combinaciones cruzadas (cross)](solucionario51/51.md)

[53. Combinaciones y funciones de agrupamiento](solucionario53/53.md)

[54. Combinar más de 2 tablas](solucionario54/54.md)

[55. Otros tipos de combinaciones](solucionario55/55.md)

[56. Clave foránea](solucionario56/56.md)

[57. Restricciones (foreign key)](solucionario57/57.md)

[58. Restricciones foreign key en la misma tabla](solucionario58/58.md)

[59. Restricciones foreign key (eliminación)](solucionario59/59.md)

[60. Restricciones foreign key deshabilitar y validar](solucionario60/60.md)

[61. Restricciones foreign key (acciones)](solucionario61/61.md)

[62. Información de user_constraints](solucionario62/62.md)

[63. Restricciones al crear la tabla](solucionario63/63.md)

[64. Unión](solucionario64/64.md)

[65. Intersección](solucionario65/65.md)

[66. Minus](solucionario66/66.md)

[67. Agregar campos (alter table - add)](solucionario67/67.md)

[68. Modificar campos (alter table - modify)](solucionario68/68.md)

[69. Eliminar campos (alter table - drop)](solucionario69/69.md)

[70.  Agregar campos y restricciones (alter table)](solucionario70/70.md)

[71. Subconsultas](solucionario71/71.md)

[72. Subconsultas como expresion](solucionario72/72.md)

[73. Subconsultas con in](solucionario73/73.md)

[75. Subconsultas correlacionadas](solucionario75/75.md)

[76. Exists y No Exists](solucionario76/76.md)

[77. Subconsulta simil autocombinacion](solucionario77/77.md)

[78. Subconsulta conupdate y delete](solucionario78/78.md)

[79. Subconsulta e insert](solucionario79/79.md)

[80. Crear tabla a partir de otra (create table-select)](solucionario80/80.md)

[81. Vistas (create view)](solucionario81/81.md)

[82. Vistas (información)](solucionario82/82.md)

[83. Vistas eliminar (drop view)](solucionario83/83.md)

[84. Vistas (modificar datos a través de ella)](solucionario84/84.md)

[85. Vistas (with read only)](solucionario85/85.md)

[86. Vistas modificar (create or replace view)](solucionario86/86.md)

[87. Vistas (with check option)](solucionario87/87.md)

[89. Vistas materializadas (materialized view)](solucionario89/89.md)

[90. Procedimientos almacenados](solucionario90/90.md)

[91. Procedimientos Almacenados (crear- ejecutar)](solucionario91/91.md)

[92. Procedimientos Almacenados (eliminar)](solucionario92/92.md)

[93. Procedimientos almacenados (parámetros de entrada)](solucionario93/93.md)

[94. Procedimientos almacenados (variables)](solucionario94/94.md)

[95. Procedimientos Almacenados (informacion)](solucionario95/95.md)

[96. Funciones](solucionario96/96.md)

[97. Control de flujo (if)](solucionario97/97.md)

[98. Control de flujo (case)](solucionario98/98.md)

[99. Control de flujo (loop)](solucionario99/99.md)

[100. Control de flujo (for)](solucionario100/100.md)

[101. Control de flujo (while loop)](solucionario101/101.md)

[102. Disparador (trigger)](solucionario102/102.md)

[103. Disparador (información)](solucionario103/103.md)

[104. Disparador de inserción a nivel de sentencia](solucionario104/104.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row)](solucionario105/105.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](solucionario106/106.md)

[107. Disparador de actualizacion a nivel de sentencia (update trigger)](solucionario107/107.md)

[108. Disparador de actualización a nivel de fila (update trigger)](solucionario108/108.md)

[109. Disparador de actualización - lista de campos (update trigger)](solucionario109/109.md)

[110. Disparador de múltiples eventos](solucionario110/110.md)

[111. Disparador (old y new)](solucionario111/111.md)

[112. Disparador condiciones (when)](solucionario112/112.md)

[113. Disparador de actualizacion - campos (updating)](solucionario113/113.md)

[114. Disparadores (habilitar y deshabilitar)](solucionario114/114.md)

[115. Disparador (eliminar)](solucionario115/115.md)

[116. Errores definidos por el usuario en trigger](solucionario116/116.md)

[117. Seguridad y acceso a Oracle](solucionario117/117.md)

[118. Usuarios (crear)](solucionario118/118.md)

[119. 	Permiso de conexión](solucionario119/119.md)

[120. Privilegios del sistema (conceder)](solucionario120/120.md)