# 33. Clave primaria compuesta

# practica laboratorio

## Ejercicio 01

### Un consultorio médico en el cual trabajan 3 médicos registra las consultas de los pacientes en una tabla llamada "consultas".

```sql
drop table consultas;
create table consultas(
    fechayhora date not null,
    medico varchar2(30) not null,
    documento char(8) not null,
    paciente varchar2(30),
    obrasocial varchar2(30),
    primary key(fechayhora, medico)
);

```
### Setee el formato de "date" para que nos muestre día, mes, año, hora y minutos:

```sql
AL.TER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

```
### Un médico sólo puede atender a un paciente en una fecha y hora determinada. En una fecha y hora determinada, varios médicos atienden a distintos pacientes. Cree la tabla definiendo una clave primaria compuesta:

```sql
create table consultas(
    fechayhora date not null,
    medico varchar2(30) not null,
    documento char(8) not null,
    paciente varchar2(30),
    obrasocial varchar2(30),
    primary key(fechayhora,medico)
);

```
### Ingrese varias consultas para un mismo médico en distintas horas el mismo día:

```sql
insert into consultas
values ('05/11/2006 8:00','Lopez','12222222','Acosta Betina','PAMI');

insert into consultas
values ('05/11/2006 8:30','Lopez','23333333','Fuentes Carlos','PAMI');

```
### Ingrese varias consultas para diferentes médicos en la misma fecha y hora:

```sql
insert into consultas
values ('05/11/2006 8:00','Perez','34444444','Garcia Marisa','IPAM');

insert into consultas
values ('05/11/2006 8:00','Duarte','45555555','Pereyra Luis','PAMI');

```
### Intente ingresar una consulta para un mismo médico en la misma hora el mismo día (mensaje de error)

```sql
insert into consultas
values (to_date('05/11/2006 8:00', 'DD/MM/YYYY HH24:MI'), 'Lopez', '12222222', 'Acosta Betina', 'PAMI');

insert into consultas
values (to_date('05/11/2006 8:30', 'DD/MM/YYYY HH24:MI'), 'Lopez', '23333333', 'Fuentes Carlos', 'PAMI');

```
### Intente cambiar la hora de la consulta de "Acosta Betina" por una no disponible ("8:30") (error)

```sql
update consultas
set fechayhora = to_date('05/11/2006 8:30', 'DD/MM/YYYY HH24:MI')
where medico = 'Lopez' and paciente = 'Acosta Betina';

```
### Cambie la hora de la consulta de "Acosta Betina" por una disponible ("9:30")

```sql
update consultas
set fechayhora = to_date('05/11/2006 9:30', 'DD/MM/YYYY HH24:MI')
where medico = 'Lopez' and paciente = 'Acosta Betina';

```

## Ejercicio 02

### Un club dicta clases de distintos deportes. En una tabla llamada "inscriptos" almacena la información necesaria.

```sql
drop table inscriptos;
create table inscriptos(
    documento char(8) not null,
    nombre varchar2(30),
    deporte varchar2(15) not null,
    año date,
    matricula char(1),
    primary key(documento, deporte, año)
);

```
### Inscriba a varios alumnos en el mismo deporte en el mismo año:

```sql
insert into inscriptos
values ('12222222','Juan Perez','tenis','2005','s');

insert into inscriptos
values ('23333333','Marta Garcia','tenis','2005','s');

insert into inscriptos
values ('34444444','Luis Perez','tenis','2005','n');

```

### Inscriba a un mismo alumno en varios deportes en el mismo año:

```sql
insert into inscriptos
values ('12222222','Juan Perez','futbol','2005','s');

insert into inscriptos
values ('12222222','Juan Perez','natacion','2005','s');

insert into inscriptos
values ('12222222','Juan Perez','basquet','2005','n');

```
### Ingrese un registro con el mismo documento de socio en el mismo deporte en distintos años:

```sql
insert into inscriptos
values ('12222222','Juan Perez','tenis','2006','s');

insert into inscriptos
values ('12222222','Juan Perez','tenis','2007','s');

```
### Intente inscribir a un socio alumno en un deporte en el cual ya esté inscripto en un año en el cual ya se haya inscripto (mensaje de error)

```sql
insert into inscriptos
values ('12222222', 'Juan Perez', 'tenis', to_date('2007', 'YYYY'), 's');

```
### Intente actualizar un registro para que la clave primaria se repita (error)

```sql
update inscriptos
set año = to_date('2005', 'YYYY')
where documento = '12222222' and deporte = 'tenis' and año = to_date('2005', 'YYYY');

```
### Muestre los nombres y años de los inscriptos en "tenis" (5 registros)

```sql
select nombre, to_char(año, 'YYYY') as año
from inscriptos
where deporte = 'tenis';

```
