# Ejercicios 
## conexion sql 
```sql
create tablespace SENATI
  datafile 'C:/app/Soporte/product/21c/oradata/XE/XEPDB1/senati01.dbf'
  size 100M
  autoextend on next 10 M
  maxsize 2G;
 
grant create table to senati;




  -- Establecer Formato de fecha - Formato 12-08-2020
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';

-- Establecer Formato de fecha - Formato 12-08-2020 13:48:05
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MON-YYYY HH24:MI:SS';

-- Establecer lenguaje del servidor
ALTER SESSION SET NLS_DATE_LANGUAGE = 'SPANISH';

 
 alter session set "_ORACLE_SCRIPT"=true;
 
CREATE USER KEMIL
IDENTIFIED BY KEMIL
DEFAULT TABLESPACE USERS
TEMPORARY TABLESPACE TEMP;
ALTER USER KEMIL QUOTA UNLIMITED ON USERS;
GRANT CREATE SESSION, CREATE VIEW TO KEMIL;
GRANT RESOURCE TO KEMIL;
ALTER USER KEMIL DEFAULT ROLE RESOURCE;

SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'KEMIL';



SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'RESOURCE';

SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'CONNECT';

grant create procedure to HR;

CREATE USER HR                           -- Se le asigna un nombre de usuario
IDENTIFIED BY HR                         -- Se le asigna una contraseña (No es necesario usar doble comillas para hacerla case sensitive)
DEFAULT TABLESPACE USERS                 -- En Oracle XE se usa el tablespace USERS para almacenar los objetos de los usuarios
TEMPORARY TABLESPACE TEMP;               -- Asignar un tablespace temporal para los datos temporales de la sesión del usuario
ALTER USER HR QUOTA UNLIMITED ON USERS;  -- Asignar una couta de almacenamiento en el tablespace USERS
GRANT CREATE SESSION TO HR;              -- Privilegio que permite que el usuario se pueda conectar a la BD (Iniciar Sesión)
GRANT CREATE VIEW TO HR;                 -- Privilegio de sistema que permite crear vistas (necesario para ejecutar script hr.sql)
GRANT RESOURCE TO HR;                    -- Asignar el rol "RESOURCE" (permite crear TABLE, SEQUENCE, TRIGGER, PROCEDURE, etc.)
ALTER USER HR DEFAULT ROLE RESOURCE;  


ALTER USER HR IDENTIFIED BY HR ACCOUNT UNLOCK;
```