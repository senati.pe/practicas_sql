# 73. Subconsultas con in

# practica laboratorio

## Ejercicio 01

### -- Trabajamos con las tablas "libros" y "editoriales" de una librería.
###  Eliminamos las tablas y las creamos:
DROP TABLE libros;
DROP TABLE editoriales;

CREATE TABLE editoriales(
    codigo number(3),
    nombre varchar2(30),
    primary key (codigo)
);

CREATE TABLE libros (
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3),
    primary key(codigo),
    constraint FK_libros_editorial
    foreign key (codigoeditorial)
    references editoriales(codigo)
);

### Ingresamos algunos registros:
INSERT INTO editoriales VALUES(1,'Planeta');
INSERT INTO editoriales VALUES(2,'Emece');
INSERT INTO editoriales VALUES(3,'Paidos');
INSERT INTO editoriales VALUES(4,'Siglo XXI');
INSERT INTO libros VALUES(100,'Uno','Richard Bach',1);
INSERT INTO libros VALUES(101,'Ilusiones','Richard Bach',1);
INSERT INTO libros VALUES(102,'Aprenda PHP','Mario Molina',4);
INSERT INTO libros VALUES(103,'El aleph','Borges',2);
INSERT INTO libros VALUES(104,'Puente al infinito','Richard Bach',2);

###  Queremos conocer el nombre de las editoriales que han publicado libros del autor "Richard Bach":
SELECT nombre
FROM editoriales
WHERE codigo IN (SELECT codigoeditorial
                 FROM libros
                 WHERE autor='Richard Bach');

### Probamos la subconsulta separada de la consulta exterior para verificar que retorna una lista de valores de un solo campo:
SELECT codigoeditorial
FROM libros
WHERE autor='Richard Bach';

###  Podemos reemplazar por un "join" la primera consulta:
SELECT DISTINCT nombre
FROM editoriales e
JOIN libros ON codigoeditorial=e.codigo
WHERE autor='Richard Bach';

###  También podemos buscar las editoriales que no han publicado libros de "Richard Bach":
SELECT nombre
FROM editoriales
WHERE codigo NOT IN (SELECT codigoeditorial
                     FROM libros
                     WHERE autor='Richard Bach');
