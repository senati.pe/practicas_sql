# 38. Restricción unique

# practica laboratorio

## Ejercicio 01

### Una empresa de remises tiene registrada la información de sus vehículos en una tabla llamada "remis".

```sql
drop table remis;
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);

```
### Ingrese algunos registros, 2 de ellos con patente repetida y alguno con patente nula.

```sql
insert into remis
values ('35','Colon','Cordoba','Cordoba');
insert into remis
values ('34','San Martin ','Cruz del Eje','Cordoba');
insert into remis
values ('65','Rivadavi ','Villa del Rosario','Cordoba');
insert into remis
values ('37','Sarmiento','Rosario','Santa Fe');
insert into remis
values ('34','San Martin ','Cruz del Eje','Cordoba');

insert into remnis
values ('34','San Martin ','Santa Fe','Santa Fe');


```

### Agregue una restricción "primary key" para el campo "numero".

```sql
ALTER TABLE remis
ADD PRIMARY KEY (documento);
```

### Intente agregar una restricción "unique" para asegurarse que la patente del remis no tomará valores repetidos.
### No se puede porque hay valores duplicados, un mensaje indica que se encontraron claves duplicadas.

```sql
UPDATE remis SET marcas = '35' WHERE numero = '34';
```




