# 5. Recuperación de registros específicos (select . where)

## practica laboratorio

### Sentencia SQL
### Trabaje con la tabla "agenda"
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);


```
### Salida 
```sql
Table AGENDA creado.
```
### Visualice la estructura de la tabla "agenda" (4 campos)
```sql
describe agenda;
```
### Salida 
```sql
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(30) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 
```
### Ingrese los siguientes registros ("insert into")
```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');

```
### Salida 
```sql
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
### Seleccione el registro cuyo nombre sea "Marisa" (1 registro)
```sql
select nombre from agenda where nombre='Marisa';

```
### Salida
```sql
nombre
Marisa
```
### Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)
```sql
select nombre,domicilio from agenda where apellido='Lopez';
```
### Salida
```sql
nombre  domicilio
Hector	Salta 545
Luis	Urquiza 333
Marisa	Urquiza 333
```
### Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)
```sql
select nombre from agenda where telefono='4545454';
```
### Salida
```sql
nombre
Luis
Marisa
```
## Ejercicio 02
### Cree la tabla "articulos"
```sql
drop table if exists articulos;

create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2)
);

```
### Salida
```sql
Table ARTICULOS creado.

```
###  ea la estructura de la tabla
```sql
 describe articulos;

```
### Salida
```sql
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CÓDIGO             NUMBER(38)   
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             FLOAT(126)  
```
### Ingrese algunos registros
```sql
insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
insert into articulos (codigo, nombre, descripcion, precio) values (4,'teclado','ingles Biswal',100);
insert into articulos (codigo, nombre, descripcion, precio) values (5,'teclado','español Biswal',90);

```
### Salida
```sql
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
### Seleccione todos los datos de los registros cuyo nombre sea "impresora" (2 registros)
```sql
select codigo,nombre,descripcion,precio from articulos3 where nombre='impresora';

```
### Salida 
```sql       
1	   impresora	Epson Stylus C45	400,8
2	   impresora	Epson Stylus C85	500
```
