# 84. Vistas (modificar datos a través de ella)

# practica laboratorio

## Ejercicio 01

### Una empresa almacena la información de sus empleados en dos tablas llamadas "empleados" y "secciones".
Eliminamos las tablas:

drop table empleados;
drop table secciones;


Creamos las tablas:

create table secciones(
    codigo number(2),
    nombre varchar2(20),
    constraint PK_secciones primary key (codigo)
);

create table empleados(
    legajo number(4) not null,
    documento char(8),
    sexo char(1),
    constraint CK_empleados_sexo check (sexo in ('f','m')),
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    seccion number(2) not null,
    cantidadhijos number(2),
    constraint CK_empleados_hijos check (cantidadhijos>=0),
    estadocivil char(10),
    constraint CK_empleados_estadocivil check (estadocivil in ('casado','divorciado','soltero','viudo')),
    fechaingreso date,
    constraint PK_empleados primary key (legajo),
    sueldo number(6,2),
    constraint CK_empleados_sueldo check (sueldo>=0),
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo),
    constraint UQ_empleados_documento
    unique(documento)
);


Ingresamos algunos registros:

insert into secciones values(1,'Administracion');
insert into secciones values(2,'Contaduría');
insert into secciones values(3,'Sistemas');
insert into empleados values(100,'22222222','f','Lopez','Ana','Colon 123',1,2,'casado','10/10/1990',600);
insert into empleados values(101,'23333333','m','Lopez','Luis','Sucre 235',1,0,'soltero','02/10/1990',650);
insert into empleados values(103,'24444444', 'm', 'Garcia', 'Marcos', 'Sarmiento 1234', 2, 3, 'divorciado', '07/12/1998',800);
insert into empleados values(104,'25555555','m','Gomez','Pablo','Bulnes 321',3,2,'casado','10/09/1998',900);
insert into empleados values(105,'26666666','f','Perez','Laura','Peru 1254',3,3,'casado','05/09/2000',700);


Eliminamos la vista "vista_empleados":

drop view vista_empleados;


Creamos la vista "vista_empleados", que es resultado de una combinación en la cual se muestran 5 campos:

create view vista_empleados as
select (apellido||' '||e.nombre) as nombre, sexo, s.nombre as seccion, cantidadhijos
from empleados e
join secciones s
on codigo=seccion;