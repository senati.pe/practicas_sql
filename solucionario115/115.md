# 115. Disparador (eliminar)

# practica laboratorio

## Ejercicio 01

### Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario, la fecha, y el tipo de modificación que se realizó sobre la tabla "libros".
Eliminamos la tabla "libros" y la tabla "control":
```sql

drop table libros;
drop table control;

```
### Creamos las tablas con las siguientes estructuras:
```sql

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);

```
### Creamos un desencadenador que se active cuando ingresamos un nuevo registro en "libros", debe almacenar en "control" el nombre del usuario que realiza el ingreso, la fecha e "insercion" en "operacion":
```sql

create or replace trigger tr_ingresar_libros
    before insert
    on libros
    for each row
begin
   insert into control values(user,sysdate,'insercion');
end tr_ingresar_libros;
/

```
### Creamos un segundo disparador que se active cuando modificamos algún campo de "libros" y almacene en "control" el nombre del usuario que realiza la actualización, la fecha y en "operacion" coloque el nombre del campo actualizado:
```sql

create or replace trigger tr_actualizar_libros
    beforeupdate
    on libros
    for each row
begin
    if updating('codigo') then
       insert into control values(user,sysdate,'codigo');
    end if;
    if updating('titulo') then
       insert into control values(user,sysdate,'titulo');
    end if;
    if updating('autor') then
       insert into control values(user,sysdate,'autor');
    end if;
    if updating('editorial') then
       insert into control values(user,sysdate,'editorial');
    end if;
    if updating('precio') then
       insert into control values(user,sysdate,'precio');
    end if;
end tr_actualizar_libros;
/
```