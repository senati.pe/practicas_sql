# 15. Tipos de datos numéricos

# practica laboratorio

## Ejercicio 01

### Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".
```sql
  Número de Cuenta        Documento       Nombre          Saldo
 ______________________________________________________________
 1234                         25666777        Pedro Perez     500000.60
 2234                         27888999        Juan Lopez      -250000
 3344                         27888999        Juan Lopez      4000.50
 3346                         32111222        Susana Molina   1000


```

### Elimine la tabla "cuentas"
```sql
DROP TABLE cuentas;

```
### Saldo de la cuenta: valores que no superan 999999.99

```sql
create table cuentas(
    numero number(4) not null,
    documento char(8),
    nombre varchar2(30),
    saldo number(8,2),
    primary key (numero)
);

```
### Ingrese los siguientes registros:
```sql
insert into cuentas(numero,documento,nombre,saldo) values('1234','25666777','Pedro Perez',500000.60);
insert into cuentas(numero,documento,nombre,saldo) values('2234','27888999','Juan Lopez',-250000);
insert into cuentas(numero,documento,nombre,saldo) values('3344','27888999','Juan Lopez',4000.50);
insert into cuentas(numero,documento,nombre,saldo) values('3346','32111222','Susana Molina',1000);

```
### salida 
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
### Muestre el número de cuenta y saldo de todas las cuentas cuyo propietario sea "Juan Lopez" (2 registros)
```sql
insert into cuenatas (codigo,titulo,autor,editorial,precio,cantidad) values(1,'El aleph','Borges','Emece',25.60,100.2);

```
### Muestre las cuentas con saldo negativo (1 registro)
```sql
SELECT * FROM cuentas WHERE saldo < 0;

```
### salida 
2234	27888999	Juan Lopez	-250000
### Muestre todas las cuentas cuyo número es igual o mayor a "3000" (2 registros)
```sql
SELECT * FROM cuentas WHERE numero >= 3000;

```
### salida 
3344	27888999	Juan Lopez	4000,5
3346	32111222	Susana Molina	1000

## Ejercicio 02

### Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.
```sql
drop table empleados;
create table empleados(
    nombre varchar2(30),
    documento char(8),
    sexo char(1),
    domicilio varchar2(30),
    sueldobasico number(7,2),--máximo estimado 99999.99
    cantidadhijos number(2)--no superará los 99
);

```
### Ingrese algunos registros:
```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Juan Perez','22333444','m','Sarmiento 123',500,2);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Ana Acosta','24555666','f','Colon 134',850,0);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Bartolome Barrios','27888999','m','Urquiza 479',10000.80,4);

```









