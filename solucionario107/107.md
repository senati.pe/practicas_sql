# 107. Disparador de actualizacion a nivel de sentencia (update trigger)

# practica laboratorio

## Ejercicio 01

### Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se modifica un registro en la tabla "libros".
Eliminamos la tabla "libros" y la tabla "control":
```sql
drop table libros;
drop table control;

```
### Creamos la tabla con la siguiente estructura:
```sql

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);
```

### Creamos la tabla "control":
```sql

create table control(
    usuario varchar2(30),
    fecha date
);
```

### Ingresamos algunos registros en "libros":
```sql

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
### Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":
```sql

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

### Creamos un disparador a nivel de sentencia, que se dispare cada vez que se actualice un registro en "libros"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "update" sobre "libros":
```sql

create or replace trigger tr_actualizar_libros
    before update
    on libros
begin
    insert into control values(user,sysdate);
end tr_actualizar_libros;
/

```
### Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:
```sql

select *from user_triggers where trigger_name ='TR_ACTUALIZAR_LIBROS';
```