# ENUNCIADO DEL EJERCICIO 1 

### A partir del siguiente enunciado se desea realiza el modelo entidad-relación. “Una empresa vende productos a varios clientes. Se necesita conocer los datos personales de los clientes (nombre, apellidos, dni, dirección y fecha de nacimiento). Cada producto tiene un nombre y un código, así como un precio unitario. Un cliente puede comprar varios productos a la empresa, y un mismo producto puede ser comprado por varios clientes. Los productos son suministrados por diferentes proveedores. Se debe tener en cuenta que un producto sólo puede ser suministrado por un proveedor, y que un proveedor puede suministrar diferentes productos. De cada proveedor se desea conocer el RUC, nombre y dirección”. 
```sql

+-----------------+        +----------------+         +-------------------+
|     Cliente     |        |    Producto    |         |     Proveedor     |
+-----------------+        +----------------+         +-------------------+
|   - id_cliente  |        |   - id_producto   |       |    - id_proveedor  |
|   - nombre      |        |   - nombre        |       |    - nombre        |
|   - apellidos   |        |   - codigo        |       |    - ruc           |
|   - dni         |        |   - precio_unit   |       |    - direccion     |
|   - direccion   |        |                  |       +-------------------+
|   - fecha_nac   |        +----------------+
+-----------------+
        |                                      
        |                                      
        |                +------------------+
        |                |   Suministro     |
        +--------------->+------------------+
                         |                  |
                         | - id_cliente     |
                         | - id_producto   |
                         | - id_proveedor  |
                         +------------------+

```
```sql
Creación de la tabla Cliente
CREATE TABLE Cliente (
  dni VARCHAR(10) PRIMARY KEY,
  nombre VARCHAR(50),
  apellidos VARCHAR(50),
  direccion VARCHAR(100),
  fecha_nacimiento DATE
);

Creación de la tabla Producto
CREATE TABLE Producto (
  codigo INT PRIMARY KEY,
  nombre VARCHAR(50),
  precio_unitario DECIMAL(10, 2)
);

Creación de la tabla Proveedor
CREATE TABLE Proveedor (
  ruc VARCHAR(10) PRIMARY KEY,
  nombre VARCHAR(50),
  direccion VARCHAR(100)
);

Creación de la tabla Venta
CREATE TABLE Venta (
  id INT PRIMARY KEY AUTO_INCREMENT,
  dni_cliente VARCHAR(10),
  codigo_producto INT,
  fecha_venta DATE,
  cantidad INT,
  FOREIGN KEY (dni_cliente) REFERENCES Cliente (dni),
  FOREIGN KEY (codigo_producto) REFERENCES Producto (codigo)
);

Creación de la tabla Suministro
CREATE TABLE Suministro (
  codigo_producto INT,
  ruc_proveedor VARCHAR(10),
  PRIMARY KEY (codigo_producto, ruc_proveedor),
  FOREIGN KEY (codigo_producto) REFERENCES Producto (codigo),
  FOREIGN KEY (ruc_proveedor) REFERENCES Proveedor (ruc)
);

```
