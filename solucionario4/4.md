# 4. Recuperar algunos campos (select)

## practica laboratorio

### Sentencia SQL

```sql
select *from libros;
```
### Salida

```sh
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
Error en la línea: 1, columna: 14
```
### crear tabla
```sql
drop table if exists libros;

create table libros(
    titulo varchar(100),
    autor varchar(30),
    editorial varchar(15),
    precio float,
    cantidad integer
);

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',45.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta',25,200);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Planeta',15.8,200);

select titulo,precio from libros;

select editorial,cantidad from libros;

select * from libros;

```
### Salida
```sql

El aleph	Borges	Emece	45,5	100
Alicia en el pais de las maravillas	Lewis Carroll	Planeta	25	200
Matematica estas ahi	Paenza	Planeta	15,8	200

```

## Ejercicios propuestos
## Ejercicio 01

```sql
drop table if exists peliculas;

```
### Salida
```sql
Table PELICULAS creado.

```
### Registros de la tabla
```sql
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,2);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',90,2);

```
### Salida 
```sql
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
### Realice un "select" mostrando solamente el título y actor de todas las películas
```sql
select titulo,actor from peliculas;

```
### Salida
```sql
titulo              Actor
Mision imposible	Tom Cruise
Mision imposible 2	Tom Cruise
Mujer bonita	Julia R.
Elsa y Fred	China Zorrilla
```
## Ejercicio 02
### Cree la tabla
```sql
 drop table if exists empleados;

 create table empleados(
  nombre varchar(20),
  documento varchar(8), 
  sexo varchar(1),
  domicilio varchar(30),
  sueldobasico float
 );

```
### Salida
```sql
Table EMPLEADOS creado.

```
### Describe la tabla
```sql
describe empleados;

```
### Salida
```sql

Nombre       ¿Nulo? Tipo         
------------ ------ ------------ 
NOMBRE              VARCHAR2(20) 
DOCUMENTO           VARCHAR2(8)  
SEXO                VARCHAR2(1)  
DOMICILIO           VARCHAR2(30) 
SUELDOBASICO        FLOAT(126) 
```
### Ingrese registros
```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22345678','m','Sarmiento 123',300);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24345678','f','Colon 134',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Marcos Torres','27345678','m','Urquiza 479',800);

```
### Salida
```sql
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.

```
### Muestre todos los datos de los empleados.

```sql
select nombre,documento,domicilio,sexo,sueldobasico from empleados;
```
### Salida
```sql
nombre         doc         domicilio       sexo sueldo
Juan Perez	   22345678	   Sarmiento 123	m	300
Ana Acosta	   24345678	   Colon     134	f	500
Marcos Torres  27345678    Urquiza   479	m	800
```
## Ejercicio 03 
### Un comercio que vende artículos de computación registra la información de sus productos en la tabla llamada "articulos".

### Elimine la tabla si existe:
```sql
drop table if exists articulos;
```
### Cree la tabla "articulos" 
```sql
create table ARTICULOS(
  código INT ,
  nombre varchar(20),
  descripcion varchar(30),
  precio float 
 );

```
### Salida
```sql
Table ARTICULOS creado.
```
### Vea la estructura de la tabla (describe).
```sql
describe ARTICULOS;

```
### Salida
```sql
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CÓDIGO             NUMBER(38)   
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             FLOAT(126) 
```
### Ingrese algunos registros
```sql
 insert into articulos (codigo, nombre, descripcion, precio)
  values (1,'impresora','Epson Stylus C45',400.80);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (2,'impresora','Epson Stylus C85',500);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (3,'monitor','Samsung 14',800);

```
### Salida
```sql
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
### Muestre sólo el nombre, descripción y precio
```sql
select nombre,descripcion,precio from ARTICULOS;

```
### Salida
```sql
nombre      descripcion         precio
impresora	Epson Stylus C45	400,8
impresora	Epson Stylus C85	500
monitor	Samsung 14	800
```
